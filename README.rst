`deb-download-from-pool.sh` is a script to download a group of related deb
packages built from the same "src" package directly from the "pool" directory
of a Debian mirror.

It could be used to downgrade a group of packages when the latest version
introduces a regression, and previous versions are not available anymore for
the current distribution.


Examples
========

Get version from testing
------------------------

Sometimes a package gets updated in Debian unstable but it introduces
regressions, so reverting to the version in testing can be useful::

   $ lsb_release --description
   Description:	Debian GNU/Linux unstable (sid)
   $ rmadison -a $(dpkg-architecture -qDEB_BUILD_ARCH) libllvm7
   libllvm7   | 1:7-6          | testing    | amd64
   libllvm7   | 1:7.0.1~+rc2-2 | unstable   | amd64

However just specifying the version to `apt-get` is not enough if the old
version is not available anymore for unstable::

   $ sudo apt-get install libllvm7=1:7-6
   Reading package lists... Done
   Building dependency tree       
   Reading state information... Done
   E: Version '1:7-6' for 'libllvm7' was not found

The official mechanism to get packages from different suites is to use pinning_.

.. _pinning: https://wiki.debian.org/AptPreferences#Pinning-1

Without pinning_ getting packages from different suites does not work::

   $ sudo apt-get install -t testing libllvm7=1:7-6
   Reading package lists... Done
   E: The value 'testing' is invalid for APT::Default-Release as such a release is not available in the sources

With `deb-download-from-pool.sh` it is possible to get packages from *testing*
even if they are not directly available from *unstable* anymore::

   $ ./deb-download-from-pool.sh libllvm7 7-6


Get ancient version from snapshot.debian.org
--------------------------------------------

Debian provides snapshots of mirrors at http://snapshot.debian.org/ and using
these it is possible to download ancient package versions.

Specifying a snapshot mirrors makes it possible to download very old packages::

   MIRROR=http://snapshot.debian.org/archive/debian/20180904T211902Z/ \
   DEBUG_MIRROR=http://snapshot.debian.org/archive/debian-debug/20180904T210823Z/ \
   ./deb-download-from-pool.sh dash 0.5.8-2.10


Known Bugs
==========

If no version is passed the script will list available versions on the mirror,
but it will do so regardless of what binaries are actually available for the
current architecture of the host running the script.

So it may list versions which could not be downloaded for the host.

A more reliable way to list version could be something like the following::

   rmadison -a $(dpkg-architecture -qDEB_BUILD_ARCH) package_name

However this would not work with arbitrary mirrors, or snapshots.
